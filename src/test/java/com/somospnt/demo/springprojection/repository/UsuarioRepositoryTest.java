package com.somospnt.demo.springprojection.repository;

import com.somospnt.demo.springprojection.projection.UsuarioPublico;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioRepositoryTest {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Test
    public void buscarPorNombre_conNombreExistente_retornaUsuarioPublico() {
        UsuarioPublico usuario = usuarioRepository.buscarPorNombre("jose");
        assertThat(usuario).isNotNull();
    }

}
