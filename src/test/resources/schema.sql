DROP TABLE IF EXISTS curso;
DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
    id bigint PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    password VARCHAR(250) NOT NULL,
    email VARCHAR(30) UNIQUE NOT NULL,
);

CREATE TABLE curso (
    id bigint PRIMARY KEY,
    nombre VARCHAR (50) NOT NULL,
    ponderacion INT NOT NULL,
    usuario_id bigint NOT NULL,
    foreign key (usuario_id) REFERENCES usuario(id)
);

-- -----------------------------------------------------------------------------
-- usuario
-- -----------------------------------------------------------------------------
INSERT INTO usuario
(id  , nombre, password, email          ) VALUES
(1   , 'jose', '123456', 'jose@jose.com'),
(2   , 'tomi', '123456', 'tomi@tomi.com');

-- -----------------------------------------------------------------------------
-- curso
-- -----------------------------------------------------------------------------
INSERT INTO curso
(id  , nombre       , ponderacion, usuario_id) VALUES
(1   , 'Java'       , 10         , 1         ),
(2   , 'java Script', 2          , 1         );

